<?php

if ( ! function_exists( 'xtrimIT_google_map_data' ) ) :
	/***************************************************/
	/* XtrimIT Get Google Map Data- Started
	* 
	*@return strimt $html
	/***************************************************/

	function xtrimIT_google_map_data( $args = array()) {
		$default = array(
					'latitude' => '-37.809674' , 
					'longitude' => '144.954718',
					'height' => '450',
					'width' => '',
				);
		$args = array_merge($default,$args);
		extract($args);

		$html = '';

		$style = "height:{$height}px;width:{$width}px;";
		if (!empty($latitude) && !empty($longitude)) {
		
		$html = '<script type="text/javascript">

		    var TJmap;
		    var TJmarker;
		    var animation;

		    function initGmap() {
		        // set Position Coordinates
		        var TJLatLng  = new google.maps.LatLng(' . $latitude.','. $longitude .');

		        // set Map Options
		        var TJMapOptions = {
		            zoom: 14,
		            maxZoom: 15,
		            minZoom: 13,
		            scrollwheel: false,
		            center: TJLatLng,
		            mapTypeId: google.maps.MapTypeId.ROADMAP,
		            styles: [{"featureType":"water","elementType":"geometry","stylers":[{"color":"#e9e9e9"},{"lightness":17}]},
		            		{"featureType":"landscape","elementType":"geometry","stylers":[{"color":"#f5f5f5"},{"lightness":20}]},
		            		{"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"color":"#ffffff"},{"lightness":17}]},
		            		{"featureType":"road.highway","elementType":"geometry.stroke","stylers":[{"color":"#ffffff"},{"lightness":29},{"weight":0.2}]},
		            		{"featureType":"road.arterial","elementType":"geometry","stylers":[{"color":"#ffffff"},{"lightness":18}]},
		            		{"featureType":"road.local","elementType":"geometry","stylers":[{"color":"#ffffff"},{"lightness":16}]},
		            		{"featureType":"poi","elementType":"geometry","stylers":[{"color":"#f5f5f5"},{"lightness":21}]},
		            		{"featureType":"poi.park","elementType":"geometry","stylers":[{"color":"#dedede"},{"lightness":21}]},
		            		{"elementType":"labels.text.stroke","stylers":[{"visibility":"on"},{"color":"#ffffff"},{"lightness":16}]},
		            		{"elementType":"labels.text.fill","stylers":[{"saturation":36},{"color":"#333333"},{"lightness":40}]},
		            		{"elementType":"labels.icon","stylers":[{"visibility":"off"}]},
		            		{"featureType":"transit","elementType":"geometry","stylers":[{"color":"#f2f2f2"},{"lightness":19}]},
		            		{"featureType":"administrative","elementType":"geometry.fill","stylers":[{"color":"#fefefe"},{"lightness":20}]},
		            		{"featureType":"administrative","elementType":"geometry.stroke","stylers":[{"color":"#fefefe"},{"lightness":17},{"weight":1.2}]}],
		            // Map Type Control View Settings
		            mapTypeControl: true,
		            mapTypeControlOptions: {
		                style: google.maps.MapTypeControlStyle.DROPDOWN_MENU,
		            },

		            // Zoom Control View Settings
		            zoomControl:true,
		            zoomControlOptions: {
		              style:google.maps.ZoomControlStyle.SMALL,
		            },

		            // Street Control View Settings
		            streetViewControl: true,

		            // Pan Control View Settings
		            panControl: true,
		            scaleControl: true,

		        };
		        
		        // New Map Initialize
		        TJmap = new google.maps.Map(document.getElementById(\'googleMaps\'), TJMapOptions);

		        // prepare new marker

		        TJmarker = new google.maps.Marker({
		            position: TJLatLng,
		            map: TJmap,
		        });
		    }
		    google.maps.event.addDomListener(window, \'load\', initGmap);

		</script>
	    <div id="googleMaps" class="map-container" style="'.$style.'"></div>';#<!-- /google-map-->

		} else {
	    	$html = '<p>Please provide the Latitudes and Longitudes value.</p>';
		} 
		
		return $html;	
	}
endif;



///How TO use ....

$args = array(
	'latitude' =>  $xtrimIT_opt['xtrimIT_gmap_lat'], //get value from theme option or your source 
	'longitude' => $xtrimIT_opt['xtrimIT_gmap_lon'],
	'height' => $xtrimIT_opt['xtrimIT_gmap_height'],
	'width' => $xtrimIT_opt['xtrimIT_gmap_width'],
);

echo xtrimIT_google_map_data($args);